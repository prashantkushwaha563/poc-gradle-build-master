const fs = require('fs');
var archiver = require('archiver');

// 1
const out = 'dist/tosca-editor.war';

// 2
const output = fs.createWriteStream(out);
const archive = archiver('zip', {});

// 3
output.on('finish', () => {
    console.log('war (' + out + ') ' + archive.pointer() + ' total bytes');
});

// 4
archive.pipe(output);

// 5
archive.directory('dist/tosca-editor/', false);

// 6
archive.finalize();