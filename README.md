# ToscaEditorNg

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




### Deploy in weblogic 12c

### prerequisite

1. `Gradle`
2. `nodejs`
3. `Weblogic 12c`
4. `wlfullclient.jar` located in `config` folder (https://docs.oracle.com/cd/E24329_01/web.1211/e24378/jarbuilder.htm#SACLT421)

### Gradle tasks

1. `gradle test` - to execute test cases
2. `gradle build` - build the war file
3. `gradle deploy` - deploy war file in weblogic


for deployment weblogic related configurations are under `config` folder 
please check following link for weblogic configurations https://docs.oracle.com/cd/E13222_01/wls/docs90/deployment/wldeployer.html#1005385

Project enviornment variables are in `src/environments` files

## Folder structer of the project

Defined here: https://drive.google.com/file/d/16IGCMFZCaG4J5TfHY3I9TSHqqwG4cFg1/view

## Video: https://drive.google.com/file/d/1bdDOVm0F0tAMCSc_vtUIxK4BwihP4Kz8/view?usp=sharing